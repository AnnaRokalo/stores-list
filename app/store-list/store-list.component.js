'use strict';

angular.
  module('storeList').
  component('storeList', {
    templateUrl: 'store-list/store-list.template.html',
    controller: ['$http', '$scope', 'dataHolder', function StoreListController($http, $scope, dataHolder) {
      $scope.currentStoreNumber = -1;
      $scope.isShowForm = 0;
      $scope.stores =  dataHolder.getStores();
      $scope.showForm = function() {
        $scope.isShowForm = 1
      };

      $scope.addNewStore = function() {
        if ( $scope.name != '' && $scope.name!== undefined) {
          $scope.stores.push(
              {
                name: $scope.name,
                address: $scope.address,
                workMode: $scope.workMode,
                storeId: $scope.name,
                imageUrl:'',
                products: []
              }
          );
          $scope.name = '';
          $scope.address = '';
          $scope.workMode = '';
          $scope.isShowForm = 0;
        }
      };

      $scope.saveStore = function() {
        if( $scope.currentStoreNumber > -1 ){
          var id = $scope.currentStoreNumber;
          $scope.stores[id].name = $scope.name;
          $scope.stores[id].address = $scope.address;
          $scope.stores[id].workMode = $scope.workMode;
          $scope.name = '';
          $scope.address = '';
          $scope.workMode = '';
          $scope.currentStoreNumber = -1;
          $scope.isShowForm = 0;
        }
      };

      $scope.editStore = function(id) {
        $scope.currentStoreNumber = id;
        $scope.isShowForm = 1;
        $scope.name = $scope.stores[id].name;
        $scope.address = $scope.stores[id].address;
        $scope.workMode = $scope.stores[id].workMode;
      };

      $scope.deleteStore = function( id ) {
        $scope.stores.splice( id, 1 );
      };
    }]
  });