angular.
module('productList').
component('productList', {
    templateUrl: 'product-list/product-list.template.html',
    controller: ['$http', '$scope', '$routeParams', 'dataHolder',
        function ProductListController($http, $scope, $routeParams, dataHolder) {
            $scope.storeId = $routeParams.storeId;
            $scope.stores=  dataHolder.getStores();
            $scope.products = dataHolder.getProducts($scope.storeId );
            $scope.currentProductNumber = -1;

            function getStoreName(stores,id) {
                var storeName;
                stores.forEach(function(item) {
                    if( item.storeId === id)
                        storeName = item.name;
                });
                return storeName;
            };

            $scope.storeName = getStoreName( $scope.stores, $scope.storeId);
            $scope.addNewProduct = function() {
                if ( $scope.productName != '' && $scope.productName!== undefined) {
                    dataHolder.updateProducts($scope.storeId, {
                            name: $scope.productName,
                            descr: $scope.productDescr
                        });
                    $scope.productName = '';
                    $scope.productDescr = '';
                }
            };

            $scope.saveProduct = function() {
                if( $scope.currentProductNumber > -1 ){
                    var id = $scope.currentProductNumber;
                    $scope.products[id].name =  $scope.productName;
                    $scope.products[id].descr = $scope.productDescr;

                    $scope.productName = '';
                    $scope.productDescr = '';
                    $scope.currentProductNumber  = -1;
                }
            };

            $scope.editProduct = function(id) {
                $scope.currentProductNumber = id;
                $scope.productName =  $scope.products[id].name;
                $scope.productDescr = $scope.products[id].descr;
            };

            $scope.deleteProduct = function(id) {
                $scope.products.splice( id, 1 );
            };
        }
    ]
});