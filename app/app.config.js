'use strict';

angular.
module('storesApp').
config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
            when('/stores', {
                template: '<store-list></store-list>'
            }).
            when('/stores/:storeId', {
                template: '<product-list></product-list>'
            }).
            otherwise('/stores');
    }
]);
