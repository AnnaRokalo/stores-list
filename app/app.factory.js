'use strict';

angular.
module('storesApp').
factory('dataHolder', function($http){
    var stores = [
        {
            "name": "Nelva",
            "address": "Belarus, Minsk, prospect Nezavisimisti, 32",
            "workMode": "10:00-20:00",
            "storeId": "nelva",
            "products": [
                {
                    "name":"блузка",
                    "descr":"женская шелковая блузка с банктом цвета кофе с молоком"
                },
                {
                    "name":"юбка",
                    "descr":"женская шерстяная юбка черного цвета"
                },
                {
                    "name":"брюки",
                    "descr":"женские зауженные брюки в клетку"
                }
            ]
        },
        {
            "name": "Marko",
            "address": "Belarus, Minsk, Kiseleva street, 12",
            "workMode": "10:00-19:00",
            "storeId": "marko",
            "products": [
                {
                    "name":"сапоги",
                    "descr":"женские осенние сапоги цвета мокко"
                },
                {
                    "name":"туфли",
                    "descr":"женские классические туфли-лодочки, лакированные, красные"
                },
                {
                    "name":"лоферы",
                    "descr":"женские лоферы, замша, синие"
                }
            ]
        },
        {
            "name": "Mark Formelle",
            "address": "Belarus, Minsk, prospect Masherova, 16",
            "workMode": "9:00-18:00",
            "storeId": "mark-formelle",
            "products": [
                {
                    "name":"майка",
                    "descr":"женская трикотажная белая нижняя майка"
                },
                {
                    "name":"юбка",
                    "descr":"женская трикотажная юбка с узором"
                },
                {
                    "name":"брюки",
                    "descr":"женские трикотажные брюки с принтом"
                }
            ]
        }
    ];
    var products = [];

    return {
        updateProducts: function(id, product) {
            stores.forEach(function(item, i) {
                if( item.storeId === id)
                    products = item.products;
            });

            products.push(product);
        },
        getStores: function() {
            return stores;
        },
        getProducts: function(id) {
            stores.forEach(function(item, i) {
                if( item.storeId === id)
                    products = item.products;
            });
            return products;
        }
    }
});
